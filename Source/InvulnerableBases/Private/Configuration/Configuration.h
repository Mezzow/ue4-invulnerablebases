/*

ARK Server Plugin
Invulnerable Bases

Makes player structures either completely invulnerable or vulnerable only at given times.

Copyright (c) 2015 Team ARK Bar
- Mario Werner
- Daniel Gothenborg
- Alexander Hagabråten

Contact mario@108bits.de or daniel@dgw.no for any issues

*/

#ifndef _CONFIGURATION_H
#define _CONFIGURATION_H

#include <Windows.h>
#include <time.h>
#include <string>
#include <sstream>
#include <vector>
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

using namespace std;

#define SETTINGS_PATH	"InvulnerableBases.ini"
#define PATTERN_PATH	"InvulnerableBases_Patterns.ini"

static char *WeekDaysAbbr[7] = { "Mo   ", "Tue  ", "Wed  ", "Thu  ", "Fr   ", "Sa   ", "Su   " };
//								 "00:00"  "00:00"  "00:00"  "00:00"  "00:00"  "00:00"  "00:00"

class Configuration
{
public:
	bool Enabled;
	bool UnprotectedDays[8];

	int StartHour[8];
	int StopHour[8];

	float ProtectedModifier;
	float UnprotectedModifier;

	bool BroadcastOnVulnerabilityChange;
	string BroadcastMessageProtectionOn;
	string BroadcastMessageProtectionOff;

	void GetHookProcInfo(char*, string&, string&);

	bool AllowAdminStructureDamage;

	void Load();
	
	Configuration();
};

static Configuration configuration;

#endif	// _CONFIGURATIONS_H