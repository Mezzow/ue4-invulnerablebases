#include "../InvulnerableBasesPrivatePCH.h"
#include <mutex>
#include <stdint.h>

static bool Initialized = false;
template<typename T>class Hook
{

public:
	mutex Mutex;
	static enum Status {
		FAILED_ENABLING,
		FAILED_PATTERN,
		FAILED_HOOKING,
		INITIALIZED,
		UNINITIALIZED,
		DISABLED,
		ENABLED
	};

	MH_STATUS hookStatus;
	Status status;

	Hook<T>(){};


	//void Create(string strPattern, string strMask, LPVOID detour, T* original)
	void Create(char *pattern, char *mask, LPVOID detour, T* original)
	{

		/*this->pattern = new char[strPattern.size() + 1]; //strdup(strHookPattern.c_str());
		std::copy(strPattern.begin(), strPattern.end(), this->pattern);
		this->pattern[strPattern.size()] = '\0';

		this->mask = new char[strMask.size() + 1]; //strdup(strHookPattern.c_str());
		std::copy(strMask.begin(), strMask.end(), this->mask);
		this->mask[strMask.size()] = '\0';

		char message[MAX_PATH];
		sprintf_s(message, "**** %s %s", this->pattern, this->mask);
		PluginAPI::ConsoleMessage(message, ConsoleAPI::LOG_ERROR);*/

		// Constructor
		this->pattern = pattern;
		this->mask = mask;

		this->detour = detour;
		this->original = original;
		this->enabled = false;
		this->searched = false;

		this->status = Status::UNINITIALIZED;

		if (!Initialized) {
			this->hookStatus = MH_Initialize();
			if (this->hookStatus == MH_OK)
			{
				this->status = Status::INITIALIZED;
				Initialized = true;
			}
			else {
				char message[256];
				sprintf_s(message, "Failed Initializing: %d", this->hookStatus);
				PluginAPI::ConsoleMessage(message);
			}
		}
		else {
			this->status = Status::INITIALIZED;
		}
	}

	bool Enable()
	{
		if (!this->searched)
		{
			// Only search if not previously searched
			this->address = Pattern::Search(this->pattern, this->mask);
			this->searched = true;
		}

		if (this->address)
		{
			// Only hook if we found the address and we're initialized
			if (!this->hooked && Initialized)
			{
				// Only hook if not hooked already
				this->hookStatus = MH_CreateHook(this->address, this->detour, reinterpret_cast<LPVOID*>(this->original));

				if (this->hookStatus == MH_OK)
				{
					this->hooked = true;
					// Only enable if hooked
					this->hookStatus = MH_EnableHook(this->address);
					if (this->hookStatus == MH_OK)
					{
						this->status = Status::ENABLED;
						this->enabled = true;
						return true;
					}
					else
					{
						char message[256];
						sprintf_s(message, "Error enabling hook: %d", this->hookStatus);
						PluginAPI::ConsoleMessage(message);
						this->status = Status::DISABLED;
					}
				}
				else
				{
					char message[256];
					sprintf_s(message, "Error creating hook: %d", this->hookStatus);
					PluginAPI::ConsoleMessage(message);
					this->status = Status::FAILED_HOOKING;
				}
			}
			else if (this->hooked && !this->enabled)
			{
				this->hookStatus = MH_EnableHook(this->address);
				if (this->hookStatus == MH_OK)
				{
					
					this->status = Status::ENABLED;
					this->enabled = true;
					this->Mutex.unlock();
					return true;
				}
				else
				{
					PluginAPI::ConsoleMessage("Could not reenable hook");
					this->status = Status::DISABLED;
				}
			}
		}
		else
		{
			// We didnt find the address
			this->status = Status::FAILED_PATTERN;
			this->hookStatus = MH_ERROR_NOT_EXECUTABLE;
		}
		return false;
	}

	LPVOID Address()
	{
		return this->address;
	}

	bool Disable()
	{
		if (this->enabled)
		{
			this->Mutex.lock();
			this->hookStatus = MH_DisableHook(address);
			if (this->hookStatus == MH_OK)
			{
				this->status = Status::DISABLED;
				this->enabled = false;
				return true;
			}
			else
			{
				char message[256];
				sprintf_s(message, "Error disabling hook: %d", this->hookStatus);
				PluginAPI::ConsoleMessage(message);
			}
		}

		return false;
	}

	~Hook<T>(){};

private:
	/*string pattern;
	string mask;*/

	char *pattern;
	char *mask;
	LPVOID address;
	LPVOID detour;
	T* original;
	bool searched;
	bool hooked;
	bool enabled;

};


