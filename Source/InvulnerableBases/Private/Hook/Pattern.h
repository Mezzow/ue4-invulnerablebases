/*
	FindPattern & DataCompare by jlnsrk & Mi4uric3 
	Class Helper by Daniel Gothenborg (Xstasy) <daniel@dgw.no>		
 */

#include "../InvulnerableBasesPrivatePCH.h"

typedef unsigned __int64 DWORD64, *PDWORD64;

class Pattern
{
public:
	static bool DataCompare(char* base, char* signature, char* mask) {
		for (; *mask; ++mask, ++base, ++signature) {
			if (*mask == 'x' && *base != *signature) return false;
		}
		return (*mask) == 0;
	}

	static void* FindPattern(char* base, size_t size, char* signature, char* mask) {
		register unsigned char firstByte = *signature;
		char* end = base + size - strlen(mask);

		for (register char* i = base; i < end; i += sizeof(size_t)) {
			size_t x = *reinterpret_cast<size_t*>(i);

			if ((x & 0xFF) == firstByte && DataCompare(i, signature, mask)) return i;
			if ((x & 0xFF00) >> 8 == firstByte && DataCompare(i + 1, signature, mask)) return i + 1;
			if ((x & 0xFF0000) >> 2 * 8 == firstByte && DataCompare(i + 2, signature, mask)) return i + 2;
			if ((x & 0xFF000000) >> 3 * 8 == firstByte && DataCompare(i + 3, signature, mask)) return i + 3;
			if ((x & 0xFF00000000) >> 4 * 8 == firstByte && DataCompare(i + 4, signature, mask)) return i + 4;
			if ((x & 0xFF0000000000) >> 5 * 8 == firstByte && DataCompare(i + 5, signature, mask)) return i + 5;
			if ((x & 0xFF000000000000) >> 6 * 8 == firstByte && DataCompare(i + 6, signature, mask)) return i + 6;
			if ((x & 0xFF00000000000000) >> 7 * 8 == firstByte && DataCompare(i + 7, signature, mask)) return i + 7;
		}
		return nullptr;
	}

	static void* Search(char* signature, char* mask)
	{

		DWORD64 dwModuleBase = 0, dwModuleSize = 0;
		dwModuleBase = (DWORD64)GetModuleHandle(NULL);
		PIMAGE_DOS_HEADER pDOSHeader = (PIMAGE_DOS_HEADER)dwModuleBase;

		if (pDOSHeader->e_magic != IMAGE_DOS_SIGNATURE)
			return NULL;

		PIMAGE_NT_HEADERS pNTHeaders = (PIMAGE_NT_HEADERS)(dwModuleBase + pDOSHeader->e_lfanew);

		dwModuleBase += pNTHeaders->OptionalHeader.BaseOfCode;
		dwModuleSize = pNTHeaders->OptionalHeader.SizeOfCode;

		return FindPattern((char*)dwModuleBase, (size_t)dwModuleSize, signature, mask);
	}

	/*static void* Search(string pattern, string mask) {

		// Local variables
		DWORD64 dwModuleBase = 0, dwModuleSize = 0, dwFinal = 0, dwAddress = 0, dwMaskLength = 0, dwBytesFound = 0;
		const char *pPattern = pattern.c_str(), *pMask = mask.c_str();

		// Get module base address or process base address if no module is passed
		dwModuleBase = (DWORD64)GetModuleHandle(NULL);

		// Couldn't find module
		if (dwModuleBase == NULL)
			return NULL;

		// Grab DOS header
		PIMAGE_DOS_HEADER pDOSHeader = (PIMAGE_DOS_HEADER)dwModuleBase;

		// Verify DOS header
		if (pDOSHeader->e_magic != IMAGE_DOS_SIGNATURE)
			return NULL;

		// Grab NT headers
		PIMAGE_NT_HEADERS pNTHeaders = (PIMAGE_NT_HEADERS)(dwModuleBase + pDOSHeader->e_lfanew);

		// Calculate where the code section starts, as that's where we'll be scanning for signatures
		dwModuleBase += pNTHeaders->OptionalHeader.BaseOfCode;
		dwModuleSize = pNTHeaders->OptionalHeader.SizeOfCode;
		// Calculate signature length and total size of block to search in
		dwMaskLength = strlen(pMask);
		dwFinal = (dwModuleBase + dwModuleSize) - dwMaskLength;

		// Start searching for a signature
		do
		{
			// Get current opcodes
			BYTE bCurrentOpcode = *(BYTE*)(dwModuleBase);
			BYTE bSigOpcode = pPattern[dwBytesFound];
			BOOL bCompare = pMask[dwBytesFound] == 'x';

			// Make sure that this part of the signature isn't masked
			switch (bCompare)
			{
			case true:
				// Verify our signature is still valid
				if (bCurrentOpcode == bSigOpcode)
					dwBytesFound++;
				else
				{
					// Reset base back to the byte directly after we started searching for our current signature
					dwModuleBase -= (dwBytesFound - 1);
					dwBytesFound = 0;
				}
				break;
			case false:
				// Masked byte, just keep searching
				dwBytesFound++;
				break;
			};

			// Signature found
			if (dwBytesFound >= dwMaskLength)
				return (void*)((dwModuleBase - dwBytesFound) + 1);

		} while (dwModuleBase++ < dwFinal);

		// Could not find signature 
		return NULL;
	}*/
};

