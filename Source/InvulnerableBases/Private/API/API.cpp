#include "../InvulnerableBasesPrivatePCH.h"

typedef std::shared_ptr<API> ARKAPIPtr;
ARKAPIPtr ARKApi(::GetAPI(), std::mem_fn(&API::Release));
char inMessage[4096], inPluginName[128];
char *outMessage[4096], *outPluginName[128];

/*void StrToChar(string input, char *output)
{
	char in[4096];
	strcpy_s(in, input.c_str());
	*output = *in;
}*/

void PluginAPI::ConsoleMessage(string message)
{
	try {
		if (ARKApi)
		{
			strcpy_s(inMessage, message.c_str());
			*outMessage = inMessage;
			strcpy_s(inPluginName, PLUGIN_NAME);
			*outPluginName = inPluginName;

			ARKApi->ConsoleMessage(outPluginName, outMessage);
		}			
	}
	catch (...)
	{
		printf("Unhandled exception: 1");
	}	
}

void PluginAPI::ConsoleMessage(string message, ConsoleAPI::Color color)
{
	try {
		if (ARKApi)
		{
			strcpy_s(inMessage, message.c_str());
			*outMessage = inMessage;
			strcpy_s(inPluginName, PLUGIN_NAME);
			*outPluginName = inPluginName;

			ARKApi->ConsoleMessage(outPluginName, outMessage, color);
		}
	}
	catch (...)
	{
		printf("Unhandled exception: 2");
	}
}

void PluginAPI::ConsoleMessage(string message, ConsoleAPI::LogType logType)
{
	try {
		if (ARKApi)
		{
			strcpy_s(inMessage, message.c_str());
			*outMessage = inMessage;
			strcpy_s(inPluginName, PLUGIN_NAME);
			*outPluginName = inPluginName;

			ARKApi->ConsoleMessage(outPluginName, outMessage, logType);
		}
	}
	catch (...)
	{
		printf("Unhandled exception: 3");
	}
}