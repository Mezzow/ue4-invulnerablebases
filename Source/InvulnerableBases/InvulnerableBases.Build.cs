// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

namespace UnrealBuildTool.Rules
{
	public class InvulnerableBases : ModuleRules
	{
        public InvulnerableBases(TargetInfo Target)
		{
            MinFilesUsingPrecompiledHeaderOverride = 1;
            bFasterWithoutUnity = true; 
            
            PublicAdditionalLibraries.Add("c:/Projects/ARK/ARKBar/Deployment/minhook.lib");
            PublicAdditionalLibraries.Add("c:/Projects/ARK/ARKBar/Deployment/version.lib");

            PublicIncludePaths.AddRange(
                new string[] {
                    // ... add public include paths required here ...

                }
            );

			PublicIncludePaths.AddRange(
				new string[] {
					"ARK/InvulnerableBases/Public",
					// ... add public include paths required here ...
				}
				);

			PrivateIncludePaths.AddRange(
				new string[] {
					"ARK/InvulnerableBases/Private",
					// ... add other private include paths required here ...
				}
				);

			PublicDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
                    "Engine"
					// ... add other public dependencies that you statically link with here ...
				}
				);

			PrivateDependencyModuleNames.AddRange(
				new string[]
				{
					// ... add private dependencies that you statically link with here ...
				}
				);

			DynamicallyLoadedModuleNames.AddRange(
				new string[]
				{
					// ... add any modules that your module loads dynamically here ...
				}
				);
		}
	}
}